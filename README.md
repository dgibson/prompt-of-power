# prompt-of-power

A tiny tool to change your shell prompt when you have elevated
capabilities.

Traditionally, Unix shells display a prompt of `$` for regular users
and `#` for root (UID 0).  On modern Linux systems, it's possible to
use capabilities to have elevated privilege even if not UID 0.  Some
of those capabilities give privileges thar are just as dangerous as
being root (indeed, `CAP_SETUID` allows you to become root).  However,
most shells won't give any indication that you have those raised
privileges, so you'll have no warning before making a dangerous
mistake.

## Usage

For `bash` you can use `prompt-of-power` by replacing `\$` with
`$(prompt-of-power)` in `PS1`.

For example:
```
$ PATH=$PATH:~/.cargo/bin
$ PS1='$(prompt-of-power) '
$
```

If you are UID 0, this will show a `#` prompt:
```
$ sudo -s
# id -au
0
#
```

If you are a regular user with no raised capabilities, it will show a normal `$` prompt:
```
$ id -au
1000
$ capsh --print | grep IAB
Current IAB: 
$
```

If you are a regular (non UID 0) user, but have any raised
capabilities it will show a `$#` prompt:

```
$ id -au
1000
$ unshare -Uc --keep-caps
$# id -au
1000
$# capsh --print | grep IAB
Current IAB: ^cap_chown,^cap_dac_override,^cap_dac_read_search,^cap_fowner,^cap_fsetid,^cap_kill,^cap_setgid,^cap_setuid,^cap_setpcap,^cap_linux_immutable,^cap_net_bind_service,^cap_net_broadcast,^cap_net_admin,^cap_net_raw,^cap_ipc_lock,^cap_ipc_owner,^cap_sys_module,^cap_sys_rawio,^cap_sys_chroot,^cap_sys_ptrace,^cap_sys_pacct,^cap_sys_admin,^cap_sys_boot,^cap_sys_nice,^cap_sys_resource,^cap_sys_time,^cap_sys_tty_config,^cap_mknod,^cap_lease,^cap_audit_write,^cap_audit_control,^cap_setfcap,^cap_mac_override,^cap_mac_admin,^cap_syslog,^cap_wake_alarm,^cap_block_suspend,^cap_audit_read,^cap_perfmon,^cap_bpf,^cap_checkpoint_restore
```
