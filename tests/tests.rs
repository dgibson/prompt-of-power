use std::fs::metadata;
use std::process::Command;

const UNSHARE: &str = "unshare";
const EXE: &str = env!("CARGO_BIN_EXE_prompt-of-power");

#[test]
fn stat() {
    metadata(EXE).unwrap();
}

#[test]
fn unprivileged() {
    let output = Command::new(EXE).output().expect("Failed to execute");
    assert!(output.status.success());
    assert_eq!(output.stdout, b"$");
}

#[test]
fn root() {
    let output = Command::new(UNSHARE)
        .arg("-Ur")
        .arg(EXE)
        .output()
        .expect("Failed to execute");
    assert!(output.status.success());
    assert_eq!(output.stdout, b"#");
}

#[test]
fn capable() {
    let output = Command::new(UNSHARE)
        .arg("-U")
        .arg("--keep-caps")
        .arg(EXE)
        .output()
        .expect("Failed to execute");
    assert!(output.status.success());
    assert_eq!(output.stdout, b"$#");
}
