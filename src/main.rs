// SPDX-License-Identifier: GPL-2.0-or-later
//
// prompt-of-power - Tool to show raised capabilities in shell prompt
//
// Copyright Red Hat
// Author: David Gibson <david@gibson.dropbear.id.au>

use std::io::Result;

mod user;

use capctl::CapState;
use user::User;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Privilege {
    None,
    Capable,
    Root,
}

impl Privilege {
    fn current() -> Result<Self> {
        let euser = User::effective();
        if euser.is_root() {
            return Ok(Self::Root);
        }

        let caps = CapState::get_current()?;
        if !caps.permitted.is_empty() {
            return Ok(Self::Capable);
        }

        Ok(Self::None)
    }
}

fn prompt(p: Privilege) -> &'static str {
    match p {
        Privilege::None => "$",
        Privilege::Capable => "$#",
        Privilege::Root => "#",
    }
}

fn main() -> Result<()> {
    let p = Privilege::current()?;
    let prompt = prompt(p);

    print!("{}", prompt);
    Ok(())
}
