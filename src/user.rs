// SPDX-License-Identifier: GPL-2.0-or-later
//
// prompt-of-power - Tool to show raised capabilities in shell prompt
//
// Helpers for checking effective UID.
//
// Copyright Red Hat
// Author: David Gibson <david@gibson.dropbear.id.au>

use libc::{geteuid, uid_t};

pub struct User(uid_t);

impl User {
    pub fn effective() -> Self {
        let uid = unsafe { geteuid() };
        Self(uid)
    }

    pub fn is_root(&self) -> bool {
        self.0 == 0
    }
}
